# OpenML dataset: rmftsa_ctoarrivals

https://www.openml.org/d/686

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Source**: Unknown - Date unknown  
**Please cite**:   

Data Sets for 'Regression Models for Time Series Analysis' by
B. Kedem and K. Fokianos, Wiley 2002. Submitted by Kostas
Fokianos (fokianos@ucy.ac.cy) [8/Nov/02] (176k)

Note: - attribute names were generated manually
- information about data taken from here:
http://lib.stat.cmu.edu/datasets/

File: ../data/rmftsa/ctoarrivals.txt

Montly number of tourist arrivals in Cyprus starting from
January 1979 and ending in December 2000.


Information about the dataset
CLASSTYPE: numeric
CLASSINDEX: none specific

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/686) of an [OpenML dataset](https://www.openml.org/d/686). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/686/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/686/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/686/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

